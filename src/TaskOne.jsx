import './App.css';
import { Container, Row, Col, Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Fragment, useEffect, useState } from 'react';

function TaskOne() {

  const [colorArray] = useState(['black', 'blue', 'yellow', 'green', 'red', 'pink', 'violet'])
  const [index, setIndex] = useState(0)
  const [selectedColor, setSelectedColor] = useState(colorArray[index])

  const createColorManager = (e, mode) => {
    e.preventDefault()
    let tempIndex = index

    console.log(index)

    switch (mode) {

      case "GET":
        setSelectedColor(colorArray[index])
        break;
      case "NEXT":
        if (tempIndex <= 5) {
          tempIndex = tempIndex + 1
          setIndex(tempIndex)
            
        }
        break;
      case "PREV":
        if (tempIndex !== 0) {
          tempIndex = tempIndex - 1
          setIndex(tempIndex)            
        }   
        break;    
      case "RESET":
        setIndex(0)
        break;
    }
  }

  useEffect(() => {
    console.log(selectedColor)
  }, [index, selectedColor])

  return (
    <Fragment >
      <Container fluid >
        <Row >
          <Col className="col-12">
            <h1>Color Array:
              {
                colorArray.map((color) => {
                  return <span key={color} style={{ color: color }}> {color}, </span>
                })
              }
            </h1>
          </Col>
          <Col className="col-12">
            <h1>Color Manager:
              <Button variant="success" onClick={(e) => createColorManager(e, "GET")}>Get Color</Button>
              <Button variant="primary" onClick={(e) => createColorManager(e, "NEXT")}>Next Color</Button>
              <Button variant="danger" onClick={(e) => createColorManager(e, "PREV")}>Prev Color</Button>
              <Button variant="secondary" onClick={(e) => createColorManager(e, "RESET")}>Reset Color</Button>
            </h1>
            <h1>Current Selected Color: {colorArray[index]}</h1>
          </Col>
        </Row>
        <Row>
          <Col className="col-12 d-flex justify-content-center">
            <h1>Return Color {selectedColor}</h1>
          </Col>
          <Col className="col-12">
            <Container
              className="box"
              style={{ background: selectedColor }}
            >
            </Container>
          </Col>
        </Row>
      </Container>

    </Fragment>
  );
}

export default TaskOne;